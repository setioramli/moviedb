package com.example.moviedb;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;

import com.example.moviedb.data.MovieService;
import com.example.moviedb.data.api.MovieApi;
import com.example.moviedb.utils.Const;
import com.example.moviedb.utils.MainThreadBus;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Firwandi S Ramli on 9/17/2019.
 */


public class MovieApplication extends Application {

    private static MovieApplication currentApplication;
    private SharedPreferences sharedPreferences;
    private SharedPreferences ncSharedPreferences;

    public SharedPreferences getNcSharedPreferences() {
        return ncSharedPreferences;
    }

    private MovieService voucherService;

    public MovieApplication() {
        currentApplication = this;
        voucherService = new MovieService();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Const.bus = new MainThreadBus();
        setupSharedPreferences();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Ubuntu-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );


        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle bundle) {
                // new activity created; force its orientation to portrait
                activity.setRequestedOrientation(
                        ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }

            @Override
            public void onActivityStarted(Activity activity) {

            }

            @Override
            public void onActivityResumed(Activity activity) {

            }

            @Override
            public void onActivityPaused(Activity activity) {

            }

            @Override
            public void onActivityStopped(Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {

            }
        });


        //printHashKey();
    }

    public static MovieApplication getInstance(){
        return currentApplication;
    }

    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    public MovieApi getVoucherApi(){
        return voucherService.getApi();
    }

    private void setupSharedPreferences() {
        this.sharedPreferences = getSharedPreferences(MovieApplication.class.getSimpleName(),
                Context.MODE_PRIVATE);
        this.ncSharedPreferences = getSharedPreferences(MovieApplication.class.getSimpleName()+"_NC",
                Context.MODE_PRIVATE);
    }

    public void printHashKey(){
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.indivara.ceploc",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }
}