package com.example.moviedb.data.api.response;

import com.example.moviedb.data.api.beans.TopRatedMovieResp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Firwandi S Ramli on 9/17/2019.
 */


public class TrendMovieResponse {

    @SerializedName("results")
    @Expose
    private List<TopRatedMovieResp> results = new ArrayList<TopRatedMovieResp>();

    @SerializedName("page")
    private int page;

    @SerializedName("total_results")
    private int total_results;

    @SerializedName("total_pages")
    private int total_pages;

    public List<TopRatedMovieResp> getList() {
        return results;
    }

    public void setDeals(List<TopRatedMovieResp> results) {
        this.results = results;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
