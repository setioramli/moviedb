package com.example.moviedb.data.api.response;

import com.example.moviedb.data.api.beans.TrendingPersonResp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Firwandi S Ramli on 9/17/2019.
 */


public class PopularPeopleResponse {

    @SerializedName("results")
    @Expose
    private List<TrendingPersonResp> results = new ArrayList<TrendingPersonResp>();

    @SerializedName("page")
    private int page;

    @SerializedName("total_results")
    private int total_results;

    @SerializedName("total_pages")
    private int total_pages;

    public List<TrendingPersonResp> getResults() {
        return results;
    }

    public void setResults(List<TrendingPersonResp> results) {
        this.results = results;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotal_results() {
        return total_results;
    }

    public void setTotal_results(int total_results) {
        this.total_results = total_results;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }
}
