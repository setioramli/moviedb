package com.example.moviedb.data.prefs;

import android.content.SharedPreferences;

import com.example.moviedb.MovieApplication;

/**
 * Created by Firwandi S Ramli on 9/17/2019.
 */


public class PrefHelper {
    private static SharedPreferences preferences;

    private static void initPref() {
        preferences = MovieApplication.getInstance().getNcSharedPreferences();
    }

    public static void setString(PrefKey key, String value) {
        initPref();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key.toString(), value);
        editor.apply();
    }

    public static String getString(PrefKey key) {
        initPref();
        return preferences.getString(key.toString(), "");
    }

    public static void setInt(PrefKey key, int value) {
        initPref();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key.toString(), value);
        editor.apply();
    }

    public static int getInt(PrefKey key) {
        initPref();
        return preferences.getInt(key.toString(), 0);
    }

    public static void setBoolean(PrefKey key, boolean value) {
        initPref();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key.toString(), value);
        editor.apply();
    }

    public static boolean getBoolean(PrefKey key) {
        initPref();
        return preferences.getBoolean(key.toString(), false);
    }

    public static void clearPreference(PrefKey key) {
        initPref();
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(key.toString());
        editor.apply();
    }

    public static void clearAllPreferences() {
        initPref();
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }
}

