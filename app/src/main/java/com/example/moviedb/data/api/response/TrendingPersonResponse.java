package com.example.moviedb.data.api.response;

import com.example.moviedb.data.api.beans.TrendingPersonResp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Firwandi S Ramli on 9/17/2019.
 */


public class TrendingPersonResponse {

    @SerializedName("results")
    @Expose
    private List<TrendingPersonResp> results = new ArrayList<TrendingPersonResp>();

    @SerializedName("page")
    private int page;

    public List<TrendingPersonResp> getList() {
        return results;
    }

    public void setResults(List<TrendingPersonResp> results) {
        this.results = results;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
