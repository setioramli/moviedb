package com.example.moviedb.data.api.response;

import com.example.moviedb.data.api.beans.TopRatedMovieResp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Firwandi S Ramli on 9/17/2019.
 */


public class NowPlayingResponse {

    @SerializedName("results")
    @Expose
    private List<TopRatedMovieResp> results = new ArrayList<TopRatedMovieResp>();

    public List<TopRatedMovieResp> getResults() {
        return results;
    }

    public void setResults(List<TopRatedMovieResp> results) {
        this.results = results;
    }
}
