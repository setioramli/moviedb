package com.example.moviedb.data.api.response;

import com.example.moviedb.data.api.beans.TrendingTVShowResp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Firwandi S Ramli on 9/17/2019.
 */


public class TrendTVShowResponse {

    @SerializedName("results")
    @Expose
    private List<TrendingTVShowResp> results = new ArrayList<TrendingTVShowResp>();

    @SerializedName("page")
    private int page;

    public List<TrendingTVShowResp> getList() {
        return results;
    }

    public void setDeals(List<TrendingTVShowResp> results) {
        this.results = results;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
