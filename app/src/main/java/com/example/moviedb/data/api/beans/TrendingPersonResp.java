package com.example.moviedb.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class TrendingPersonResp{

	@SerializedName("gender")
	private int gender;

	@SerializedName("media_type")
	private String mediaType;

	@SerializedName("known_for_department")
	private String knownForDepartment;

	@SerializedName("popularity")
	private double popularity;

	@SerializedName("name")
	private String name;

	@SerializedName("profile_path")
	private String profilePath;

	@SerializedName("id")
	private int id;

	@SerializedName("adult")
	private boolean adult;

	@SerializedName("known_for")
	@Expose
	private List<TrendingPersonMsgResp> known = new ArrayList<TrendingPersonMsgResp>();

	public void setGender(int gender){
		this.gender = gender;
	}

	public int getGender(){
		return gender;
	}

	public void setMediaType(String mediaType){
		this.mediaType = mediaType;
	}

	public String getMediaType(){
		return mediaType;
	}

	public void setKnownForDepartment(String knownForDepartment){
		this.knownForDepartment = knownForDepartment;
	}

	public String getKnownForDepartment(){
		return knownForDepartment;
	}

	public void setPopularity(double popularity){
		this.popularity = popularity;
	}

	public double getPopularity(){
		return popularity;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setProfilePath(String profilePath){
		this.profilePath = profilePath;
	}

	public String getProfilePath(){
		return profilePath;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setAdult(boolean adult){
		this.adult = adult;
	}

	public boolean isAdult(){
		return adult;
	}

	public List<TrendingPersonMsgResp> getKnown() {
		return known;
	}

	public void setKnown(List<TrendingPersonMsgResp> known) {
		this.known = known;
	}

	@Override
 	public String toString(){
		return 
			"TrendingPersonResp{" + 
			"gender = '" + gender + '\'' + 
			",media_type = '" + mediaType + '\'' + 
			",known_for_department = '" + knownForDepartment + '\'' + 
			",popularity = '" + popularity + '\'' + 
			",name = '" + name + '\'' + 
			",profile_path = '" + profilePath + '\'' + 
			",id = '" + id + '\'' + 
			",adult = '" + adult + '\'' + 
			"}";
		}
}