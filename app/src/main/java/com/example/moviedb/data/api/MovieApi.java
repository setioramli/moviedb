package com.example.moviedb.data.api;


import com.example.moviedb.data.api.beans.TrendingMovieResp;
import com.example.moviedb.data.api.response.NowPlayingResponse;
import com.example.moviedb.data.api.response.PopularPeopleResponse;
import com.example.moviedb.data.api.response.TrendMovieResponse;
import com.example.moviedb.data.api.response.TrendTVShowResponse;
import com.example.moviedb.data.api.response.TrendingPersonResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * Created by Firwandi S Ramli on 9/17/2019.
 */


public interface MovieApi {

   /* @GET("trending/movie/day")
    Call<TrendMovieResponse> getTrendMovieList(@Query("api_key") String apiKey
    );*/

    @GET("tv/top_rated")
    Call<TrendTVShowResponse> getTrendTVShow(@Query("api_key") String apiKey
    );

    @GET("trending/person/day")
    Call<TrendingPersonResponse> getTrendPerson(@Query("api_key") String apiKey
    );

    @GET("person/popular")
    Call<PopularPeopleResponse> getPopularPerson(@Query("api_key") String apiKey
    );


    @GET("movie/top_rated")
    Call<TrendMovieResponse> getTopRatedMovie(@Query("api_key") String apiKey
    );

    @GET("movie/now_playing")
    Call<NowPlayingResponse> getListNowPlaying(@Query("api_key") String apiKey
    );
}
