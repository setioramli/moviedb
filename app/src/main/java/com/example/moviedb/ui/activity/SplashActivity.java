package com.example.moviedb.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.moviedb.BaseActivity;
import com.example.moviedb.BuildConfig;
import com.example.moviedb.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

public class SplashActivity extends BaseActivity {

    @Override
    protected int getLayout() {
        return R.layout.a_splash;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TextView tv = (TextView)findViewById(R.id.tv);
        ImageView iv = (ImageView)findViewById(R.id.splashBg);
        tv.setText(getResources().getString(R.string.app_name));

        Picasso.with(getApplicationContext()).load(R.drawable.videocamera_white).fit()
                .into(iv, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError() {
                    }
                });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                finish();
                HomeActivity.startActivity(SplashActivity.this);

            }
        }, 3000);


    }
}
