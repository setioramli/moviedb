package com.example.moviedb.ui.presenter;

import android.view.View;

import com.example.moviedb.R;
import com.example.moviedb.data.api.MovieApi;
import com.example.moviedb.data.api.beans.TopRatedMovieResp;
import com.example.moviedb.data.api.response.NowPlayingResponse;
import com.example.moviedb.ui.fragment.NowPlayingFragment;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Firwandi S Ramli on 9/17/2019.
 */


public class NowPlayingPresenter{

        private NowPlayingFragment mFragment;

        public NowPlayingPresenter(NowPlayingFragment mFragment) {
            this.mFragment = mFragment;
        }

        public void presentGetNowPlayingList() {
            mFragment.rvList.setVisibility(View.GONE);
            mFragment.progress.setVisibility(View.VISIBLE);
            mFragment.progressMessage.setText(mFragment.getResources().getString(R.string.loading));

            MovieApi api = mFragment.getMovieApi();
            Call<NowPlayingResponse> dealsDetailResponseCall = api.getListNowPlaying("b8f4a5140ec820489a2f2b266e17d957");
            dealsDetailResponseCall.enqueue(new Callback<NowPlayingResponse>() {
                @Override
                public void onResponse(Call<NowPlayingResponse> call, Response<NowPlayingResponse> response) {

                    if (response.code() == 200) {
                        System.out.println("==========200===========");
                        NowPlayingResponse resp = response.body();
                        if (response.body() != null) {
                            if (!resp.getResults().isEmpty()) {
                                initList(resp.getResults());
                                mFragment.rvList.setVisibility(View.VISIBLE);
                            } else {
                                if (mFragment != null && mFragment.isAdded()) {
                                    String message = mFragment.getString(R.string.no_data);
                                    mFragment.noDataState.setVisibility(View.VISIBLE);
                                    mFragment.noDataMessage.setText(message);
                                }
                            }
                        } else {
                            String message = mFragment.getString(R.string.no_data);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }

                    } else {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.no_data);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    }
                    mFragment.progress.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<NowPlayingResponse> call, Throwable t) {
                    System.out.println("==========ONFAILURE=========");
                    mFragment.progress.setVisibility(View.GONE);
                }
            });
        }


        protected void initList(List<TopRatedMovieResp> list) {
            mFragment.mList.clear();
            mFragment.mList.addAll(list);
            mFragment.mAdapter.notifyDataSetChanged();
            mFragment.mAdapter.setLoaded(false);
            mFragment.page++;
        }
}