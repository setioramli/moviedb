package com.example.moviedb.ui.presenter;

import android.view.View;

import com.example.moviedb.R;
import com.example.moviedb.data.api.MovieApi;
import com.example.moviedb.data.api.beans.TrendingPersonResp;
import com.example.moviedb.data.api.response.PopularPeopleResponse;
import com.example.moviedb.ui.fragment.PopularPersonFragment;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Firwandi S Ramli on 9/17/2019.
 */


public class PopularPersonPresenter {
    private PopularPersonFragment mFragment;

    public PopularPersonPresenter(PopularPersonFragment mFragment) {
        this.mFragment = mFragment;
    }

    public void presentGetTrendingMovieList() {
        mFragment.rvList.setVisibility(View.GONE);
        mFragment.progress.setVisibility(View.VISIBLE);
        mFragment.progressMessage.setText(mFragment.getResources().getString(R.string.loading));

        MovieApi api = mFragment.getMovieApi();
        Call<PopularPeopleResponse> dealsDetailResponseCall = api.getPopularPerson("b8f4a5140ec820489a2f2b266e17d957");
        dealsDetailResponseCall.enqueue(new Callback<PopularPeopleResponse>() {
            @Override
            public void onResponse(Call<PopularPeopleResponse> call, Response<PopularPeopleResponse> response) {

                if (response.code() == 200) {
                    System.out.println("==========200===========");
                    PopularPeopleResponse resp = response.body();
                    if (response.body() != null) {
                        if (!resp.getResults().isEmpty()) {
                            initList(resp.getResults());
                            mFragment.rvList.setVisibility(View.VISIBLE);
                        } else {
                            if (mFragment != null && mFragment.isAdded()) {
                                String message = mFragment.getString(R.string.no_data);
                                mFragment.noDataState.setVisibility(View.VISIBLE);
                                mFragment.noDataMessage.setText(message);
                            }
                        }
                    } else {
                        System.out.println("=========BODY NULL=============");
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                    }

                } else {
                    if (mFragment != null && mFragment.isAdded()) {
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                    }
                }
                mFragment.progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<PopularPeopleResponse> call, Throwable t) {
                System.out.println("==========ONFAILURE=========");
                mFragment.progress.setVisibility(View.GONE);
            }
        });
    }


    protected void initList(List<TrendingPersonResp> list) {
        mFragment.mList.clear();
        mFragment.mList.addAll(list);
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }
}
