package com.example.moviedb.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.moviedb.R;
import com.example.moviedb.data.api.beans.TrendingPersonResp;
import com.example.moviedb.ui.BaseFragment;
import com.example.moviedb.ui.adapter.PopularPersonAdapter;
import com.example.moviedb.ui.presenter.PopularPersonPresenter;
import com.example.moviedb.utils.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Firwandi S Ramli on 9/17/2019.
 */


public class PopularPersonFragment extends BaseFragment {

    private static final String TAG = TrendingMovieFragment.class.getSimpleName();

    public TextView noDataMessage;
    public LinearLayout noDataState;
    public RecyclerView rvList;
    public TextView progressMessage;
    public LinearLayout progress;
    public ImageView networkProblemImage;
    public PopularPersonAdapter mAdapter;
    public PopularPersonPresenter mPresenter;

    public List<TrendingPersonResp> mList = new ArrayList<>();

    public int page = 0;

    @Override
    protected int getLayout() {
        return R.layout.f_trending_movie;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mPresenter = new PopularPersonPresenter(this);

        rvList = (RecyclerView) getActivity().findViewById(R.id.GeneralRVList);
        progressMessage = (TextView) getActivity().findViewById(R.id.progress_message);
        progress = (LinearLayout) getActivity().findViewById(R.id.progress);
        networkProblemImage = (ImageView) getActivity().findViewById(R.id.network_problem_image);
        noDataMessage = (TextView) getActivity().findViewById(R.id.no_data_state_message);
        noDataState = (LinearLayout) getActivity().findViewById(R.id.no_data_state);

        mPresenter.presentGetTrendingMovieList();


    }

    @Override
    public void onResume() {
        super.onResume();
        page = 1;
        initRVListGruping();
        mPresenter.presentGetTrendingMovieList();
    }

    protected void initRVListGruping() {
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvList.setLayoutManager(linearLayoutManager);
        mAdapter = new PopularPersonAdapter(this, mList, rvList);
        rvList.setItemAnimator(new DefaultItemAnimator());
        rvList.setAdapter(mAdapter);
        rvList.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.SimpleOnItemClickListener() {
            @Override
            public void onItemClick(View childView, int position) {
                super.onItemClick(childView, position);

                //ToDo Something here When Item Clicked
            }

        }));

    }
}