package com.example.moviedb.ui.adapter;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.moviedb.R;
import com.example.moviedb.data.api.beans.TrendingPersonResp;
import com.example.moviedb.ui.fragment.TrendingPersonFragment;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Firwandi S Ramli on 9/17/2019.
 */


public class TrendingPersonAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private TrendingPersonFragment mContext;
    private List<TrendingPersonResp> mDataset;

    // The minimum amount of items to have below your current scroll position before loading more.
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OneLoadMoreListener onLoadMoreListener;

    public interface OneLoadMoreListener {
        void onLoadMore();
    }

    public TrendingPersonAdapter(TrendingPersonFragment context, List<TrendingPersonResp> myDataSet, RecyclerView recyclerView) {
        mContext = context;
        mDataset = myDataSet;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mDataset.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_trending_person, parent, false);

            vh = new ListVoucherHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.progressbar_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ListVoucherHolder) {
            final TrendingPersonResp item = mDataset.get(position);

            ((ListVoucherHolder) holder).itemView.setTag(item);
            ((ListVoucherHolder) holder).pbLoading.setVisibility(View.VISIBLE);
            ((ListVoucherHolder) holder).tvName.setText(" " + item.getName());
            ((ListVoucherHolder) holder).tvName2.setText(" " + item.getKnown().get(0).getTitle());
            ((ListVoucherHolder) holder).tvName3.setText(" " + item.getKnown().get(1).getTitle());

            ((ListVoucherHolder) holder).tvRate.setText("Popularity : " + item.getPopularity());
            ((ListVoucherHolder) holder).tvReleaseDate.setText("Specialization : " + item.getKnownForDepartment());

            if (item != null && !item.getKnown().get(0).getPosterPath().equals("")) {
                Picasso.with(mContext.getContext()).load("https://image.tmdb.org/t/p/w500" + item.getKnown().get(0).getPosterPath()).placeholder(R.drawable.image_placeholder)
                        .centerCrop().fit().into(((ListVoucherHolder) holder).civLogo, new Callback() {
                    @Override
                    public void onSuccess() {
                        ((ListVoucherHolder) holder).pbLoading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        ((ListVoucherHolder) holder).pbLoading.setVisibility(View.GONE);
                        ((ListVoucherHolder) holder).civLogo.setImageResource(R.drawable.image_placeholder);
                    }
                });
            } else {
                ((ListVoucherHolder) holder).civLogo.setImageResource(R.drawable.image_placeholder);
                ((ListVoucherHolder) holder).pbLoading.setVisibility(View.GONE);
            }

        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void setLoaded(boolean status) {
        loading = status;
    }

    public void setOnLoadMoreListener(OneLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    public static class ListVoucherHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        TextView tvReleaseDate;
        TextView tvRate;
        TextView tvName2;
        TextView tvName3;
        ImageView civLogo;
        ProgressBar pbLoading;

        public ListVoucherHolder(View itemView) {
            super(itemView);

            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvName2 = (TextView) itemView.findViewById(R.id.tvName2);
            tvName3 = (TextView) itemView.findViewById(R.id.tvName3);
            tvReleaseDate = (TextView) itemView.findViewById(R.id.tvReleaseDate);
            tvRate = (TextView) itemView.findViewById(R.id.tvRate);
            civLogo = (ImageView) itemView.findViewById(R.id.imgView);
            pbLoading = (ProgressBar) itemView.findViewById(R.id.vcLoading);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        }
    }
}
