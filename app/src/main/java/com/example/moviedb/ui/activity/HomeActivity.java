package com.example.moviedb.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.moviedb.BaseActivity;
import com.example.moviedb.R;
import com.example.moviedb.ui.fragment.NowPlayingFragment;
import com.example.moviedb.ui.fragment.PopularPersonFragment;
import com.example.moviedb.ui.fragment.TrendingMovieFragment;
import com.example.moviedb.ui.fragment.TrendingPersonFragment;
import com.example.moviedb.ui.fragment.TrendingTVShowFragment;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends BaseActivity {

    CircleImageView civLogo;
    BottomNavigationViewEx navigation;


    boolean doubleBackToExitPressedOnce = false;

    public static void startActivity(BaseActivity sourceActivity) {
        Intent i = new Intent(sourceActivity, HomeActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        sourceActivity.startActivity(i);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_home;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        civLogo = (CircleImageView) findViewById(R.id.civLogo);
        navigation = (BottomNavigationViewEx) findViewById(R.id.bnve);

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.enableAnimation(false);
        navigation.enableItemShiftingMode(false);
        navigation.enableShiftingMode(false);
        navigation.setTextVisibility(true);
        navigation.setCurrentItem(0);

    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            android.support.v4.app.Fragment fragment;
            switch (item.getItemId()) {
                case R.id.nav_home:

                    fragment = new TrendingMovieFragment();
                    loadFragment(fragment);
                    return true;

                case R.id.nav_voucherku:

                        fragment = new TrendingTVShowFragment();
                        loadFragment(fragment);
                        return true;

                case R.id.nav_reward:

                        fragment = new TrendingPersonFragment();
                        loadFragment(fragment);
                        return true;


                case R.id.nav_history:

                        fragment = new PopularPersonFragment();
                        loadFragment(fragment);
                        return true;

                case R.id.nav_akun:

                        fragment = new NowPlayingFragment();
                        loadFragment(fragment);
                        return true;


            }
            return false;
        }
    };

    @OnClick({R.id.imgBtnCart, R.id.imgBtnSearch, R.id.home_btnQR, R.id.imgBtnShare, R.id.civLogo})
    public void onViewClicked(View view) {

        android.support.v4.app.Fragment fragment;

        switch (view.getId()) {
            case R.id.imgBtnCart:

                showToast("This Feature not ready yet");

                break;
            case R.id.imgBtnSearch:

                showToast("This Feature not ready yet");

                break;

            case R.id.home_btnQR:

                showToast("This Feature not ready yet");

                break;

            case R.id.imgBtnShare:

                showToast("This Feature not ready yet");

                break;

            case R.id.civLogo:

                showToast("Tap Here to change Photo, We're sorry This Feature not ready yet");

                break;
        }
    }

    private void loadFragment(android.support.v4.app.Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
//        transaction.addToBackStack(null);
        transaction.commit();
    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        showToast("Klik sekali lagi untuk keluar");

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
